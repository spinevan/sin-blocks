import { IconButton, TextControl } from '@wordpress/components';
import { MediaPlaceholder } from '@wordpress/block-editor';


export class PresentationCard extends React.Component {

	constructor(props) {
		super(props);
		this.state = { modalIconOpen: false, title: props.title };
		this.handleClickDelete = this.handleClickDelete.bind(this);
		this.handleSelectIcon = this.handleSelectIcon.bind(this);
		this.handleChangeTitle = this.handleChangeTitle.bind(this);
	}

	// handleChange(event) {
	//
	// 	//console.log(event.target.value);
	//
	// 	this.setState({ value: event.target.value });
	// 	this.props.onEdit(event.target.value);
	// }

	handleClickDelete = () => {
		this.setState( { modalIconOpen: false } );
		this.props.onDelete(this.props.id);
	}

	handleSelectIcon ( el ) {
		//this.props.setAttributes( { blockPhoto: el.url } )
		//console.log(el.url);
		this.setState( { modalIconOpen: false } );
		this.props.onChangeIcon(this.props.id, el.url);
	}

	handleChangeTitle ( el ) {
		this.setState( { title: el } );
		this.props.onChangeTitle(this.props.id, el);
	}

	render() {

		const openModal = () => this.setState( { modalIconOpen: true } );
		const closeModal = () => this.setState( { modalIconOpen: false } );

		return (
			<div className="presentation__cards__item">
				<img src={this.props.src} alt="icon" onClick={ openModal } />
				<TextControl
					value={ this.state.title }
					onChange={ this.handleChangeTitle }
				/>
				<IconButton	icon="minus" label="Удалить карточку" onClick={this.handleClickDelete} />

				{ this.state.modalIconOpen && (
					<MediaPlaceholder
						onSelect={ this.handleSelectIcon }
						allowedTypes={ [ 'image' ] }
						multiple={ false }
						labels={ { title: 'Редактирование иконки' } }
						addToGallery={ true }
					>
						<IconButton	icon="dismiss" label="Закрыть выбор иконки" onClick={closeModal} />
					</MediaPlaceholder>
				) }


			</div>
		);
	}
}
