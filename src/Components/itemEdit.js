export class ItemEdit extends React.Component {

	constructor(props) {
		super(props);
		this.state = { value: this.props.value };
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(event) {

		//console.log(event.target.value);

		this.setState({ value: event.target.value });
		this.props.onEdit(event.target.value);
	}

	render() {
		//console.log(this.props.item);

		//console.log('Рендер');

		return (
			<li>
				<label>{this.props.label}
					<input type="text" value={this.state.value} onChange={this.handleChange} />
				</label>
			</li>
		);
	}
}
