import { ItemEdit } from './itemEdit';

export class LinkEditor extends React.Component {
	constructor(props) {
		super(props);
		this.state = { onEdit: false };
		this.onClickEdit = this.onClickEdit.bind(this);

	}

	onClickEdit() {
		this.setState({ onEdit: !this.state.onEdit });
	}


	render() {

		let button;
		if (this.state.onEdit) {
			button = <button onClick={this.onClickEdit}> Close  </button>
		} else {
			button =  <button onClick={this.onClickEdit}> Edit  </button>
		}



		return(
			<div className="sin-link-editor">
				{button}
				<ul>
					{/*{  this.state.onEdit &&	<ItemEdit /> }*/}
					{/*{listItems}*/}
					{/*<ItemEdit />*/}

					{/*{this.props.fields.map((number) =>*/}
					{/*<ListItem key={number.label}*/}
								  {/*value={number.label} />*/}
					{/*)}*/}

					{this.state.onEdit && this.props.fields.map((item, index) =>
						<ItemEdit key={index} label={item.label} value={item.value} onEdit={item.func} />
					)}

				</ul>
			</div>
		);
	}
}
