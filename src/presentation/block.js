/**
 * BLOCK: react-lifecycle-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';
import { RichText, InspectorControls, MediaPlaceholder } from '@wordpress/block-editor';
import { PanelBody, IconButton } from '@wordpress/components';
import { PresentationCard } from '../Components/presentationCard';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { Component } = wp.element; // Import the Component base class from the React.js abstraction


/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'sin/presentation', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'sin-blocks - presentation' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'sin-blocks — CGB Block' ),
		__( 'sin CGB Example' ),
		__( 'create-guten-block' ),
	],
	supports: {
		align: ['full']
	},
	attributes: {
		blockTitle: {
			type: 'string',
			default: 'Making Wireframes Especially Useful, Even Necessary.'
		},
		blockDetail: {
			type: 'string',
			default: 'Тут должен быть текст презентации. reader will be distracted by the readable content of that pages when looking for the layout cut.'
		},
		blockPhoto: {
			type: 'string',
			default: 'images/media-button-2x.png'
		},
		presentationCards: {
			type: 'array',
			default: [{ title: 'kard1', src: 'images/media-button.png' }, { title: 'kard2', src: 'images/media-button.png' }]
		}
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: class extends Component {
		//standard constructor for a component
		constructor() {
			super( ...arguments );

			// example how to bind `this` to the current component for our callbacks
			this.onTitleChange = this.onTitleChange.bind( this );
			this.onDetailChange = this.onDetailChange.bind( this );
			this.onSelectPhoto = this.onSelectPhoto.bind( this );
			this.onAddPresentationCard = this.onAddPresentationCard.bind( this );
			this.onDeleteCard = this.onDeleteCard.bind( this );
			this.onChangeIcon = this.onChangeIcon.bind( this );
			this.onChangeTitle = this.onChangeTitle.bind( this );

			// some place for your state
			this.state = { presentationCards : [...this.props.attributes.presentationCards] };
		}

		onTitleChange( newContent ) {
			this.props.setAttributes( { blockTitle : newContent } );
		}

		onDetailChange( newContent ) {
			this.props.setAttributes( { blockDetail : newContent } );
		}

		onSelectPhoto( el ) {
			this.props.setAttributes( { blockPhoto: el.url } )
		}

		onAddPresentationCard() {
			const arrCards = this.state.presentationCards;
			arrCards.push({ title: 'new Card', src: 'images/media-button.png' });
			this.setState(
				{ presentationCards: arrCards }
			);
			this.props.setAttributes( { presentationCards : [...arrCards] } );
			//console.log( this.props.attributes.presentationCards, ': добавили карточку' );
		}

		onDeleteCard( id ) {
			//console.log(e);
			const arrCards = this.state.presentationCards;
			arrCards.splice(id, 1);
			this.setState(
				{ presentationCards: arrCards }
			);
			this.props.setAttributes( { presentationCards : [...arrCards] } );
		}

		onChangeIcon( id, url) {
			const arrCards = this.state.presentationCards;
			arrCards[ id ].src = url;
			this.setState(
				{ presentationCards: arrCards }
			);
			this.props.setAttributes( { presentationCards : [...arrCards] } );
		}

		onChangeTitle( id, title) {
			const arrCards = this.state.presentationCards;
			arrCards[ id ].title = title;
			this.setState(
				{ presentationCards: arrCards }
			);
			this.props.setAttributes( { presentationCards : [...arrCards] } );
		}


		// edit: function (props) {
		// Creates a <p class='wp-block-cgb-block-react-lifecycle-block'></p>.
		render() {
			return (
				<section>

					<div className="container presentation">
						<RichText tagName="h2" className="title_vertical" onChange={ this.onTitleChange } value={ this.props.attributes.blockTitle } />
						<div className="col-60">
							<RichText tagName="p" onChange={ this.onDetailChange } value={ this.props.attributes.blockDetail } />
							<div className="presentation__cards">
								{this.state.presentationCards.map((item, index) =>
									<PresentationCard key={index} title={item.title} src={item.src} id={index}
										onDelete={this.onDeleteCard}
										onChangeIcon={this.onChangeIcon}
										onChangeTitle={this.onChangeTitle} />
								)}
							</div>
							<IconButton	icon="plus-alt"	label="Добавить карточку" onClick={this.onAddPresentationCard} />

						</div>

						<div  className="col-30">
							<img src={this.props.attributes.blockPhoto} alt="foto" />
						</div>

					</div>

					<InspectorControls>
						<PanelBody title={ __( 'Картинка в правой колонке' ) }>
							<MediaPlaceholder
								onSelect={ this.onSelectPhoto }
								allowedTypes={ [ 'image' ] }
								multiple={ false }
								labels={ { title: 'Картинка в правой колонке' } }
							>
								Эта картинка будет отображаться в правой колонке
							</MediaPlaceholder>
						</PanelBody>
					</InspectorControls>

				</section>
			);
		}
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 * @return {string}  The block,
	 * @param {object} settings Block settings
	 */
	save: ( props ) => {
		return (
			<section>

				<div className="container presentation">
					<RichText.Content tagName="h2" className="title_vertical" value={ props.attributes.blockTitle } />
					<div className="col-60">
						<RichText.Content tagName="p"  value={ props.attributes.blockDetail } />
						<div className="presentation__cards">
							{props.attributes.presentationCards.map((item, index) =>
								<div className="presentation__cards__item" key={index}>
									<img src={item.src} alt="icon" />
									<p>{item.title}</p>
								</div>
							)}
						</div>
					</div>

					<div className="col-30">
						<img src={ props.attributes.blockPhoto } alt="foto" />
					</div>

				</div>

			</section>
		);
	},
} );
