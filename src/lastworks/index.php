<?php
/**
 * Created by PhpStorm.
 * User: sinicyn
 * Date: 30.01.2020
 * Time: 10:45
 */

namespace Sin\Blocks\LastWorks;

add_action('plugins_loaded', __NAMESPACE__ . '\register_dynamic_block');

add_action( 'init',  __NAMESPACE__ .'\register_sin_work_post_type' );
function register_sin_work_post_type() {
    // Раздел вопроса - faqcat
    register_taxonomy('sinworkcat', array('sinwork'), array(
        'label'                 => 'Раздел работ', // определяется параметром $labels->name
        'labels'                => array(
            'name'              => 'Разделы работ',
            'singular_name'     => 'Раздел работы',
            'search_items'      => 'Искать Раздел работ',
            'all_items'         => 'Все Разделы работ',
            'parent_item'       => 'Родит. раздел работы',
            'parent_item_colon' => 'Родит. раздел работы:',
            'edit_item'         => 'Ред. Раздел работы',
            'update_item'       => 'Обновить Раздел работы',
            'add_new_item'      => 'Добавить Раздел работы',
            'new_item_name'     => 'Новый Раздел работы',
            'menu_name'         => 'Разделы работ',
        ),
        'show_in_rest'        => true,
        'description'           => 'Рубрики для раздела работ', // описание таксономии
        'public'                => true,
        'show_in_nav_menus'     => false, // равен аргументу public
        'show_ui'               => true, // равен аргументу public
        'show_tagcloud'         => true, // равен аргументу show_ui
        'hierarchical'          => true,
        'rewrite'               => array('slug'=>'work', 'hierarchical'=>false, 'with_front'=>false, 'feed'=>false ),
        'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
    ) );

    // тип записи - вопросы - faq
    register_post_type('sinwork', array(
        'label'               => 'Работы',
        'labels'              => array(
            'name'          => 'Работы',
            'singular_name' => 'Работа',
            'menu_name'     => 'Работы и проекты',
            'all_items'     => 'Все работы',
            'add_new'       => 'Добавить работу',
            'add_new_item'  => 'Добавить новую работу',
            'edit'          => 'Редактировать',
            'edit_item'     => 'Редактировать работу',
            'new_item'      => 'Новая работа',
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_rest'        => true,
        'rest_base'           => '',
        'show_in_menu'        => true,
        'exclude_from_search' => false,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
        'hierarchical'        => false,
        'rewrite'             => array( 'slug'=>'work/%sinworkcat%', 'with_front'=>false, 'pages'=>false, 'feeds'=>false, 'feed'=>false ),
        'has_archive'         => 'works',
        'query_var'           => true,
        'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
        'taxonomies'          => array( 'sinworkcat' ),
    ) );

}

## Отфильтруем ЧПУ произвольного типа
// фильтр: apply_filters( 'post_type_link', $post_link, $post, $leavename, $sample );
add_filter('post_type_link', __NAMESPACE__ .'\sinwork_permalink', 1, 2);
function sinwork_permalink( $permalink, $post ){
    // выходим если это не наш тип записи: без холдера %products%
    if( strpos($permalink, '%sinworkcat%') === false )
        return $permalink;

    // Получаем элементы таксы
    $terms = get_the_terms($post, 'sinworkcat');
    // если есть элемент заменим холдер
    if( ! is_wp_error($terms) && !empty($terms) && is_object($terms[0]) )
        $term_slug = array_pop($terms)->slug;
    // элемента нет, а должен быть...
    else
        $term_slug = 'no-sinworkcat';

    return str_replace('%sinworkcat%', $term_slug, $permalink );
}




function register_dynamic_block() {
    // Only load if Gutenberg is available.
    if (!function_exists('register_block_type')) {
        return;
    }

    // Hook server side rendering into render callback
    // Make sure name matches registerBlockType in ./index.js
    register_block_type('sin/lastworks', array(
        'render_callback' => __NAMESPACE__ . '\render_dynamic_block'
    ));
}

function render_dynamic_block($attributes) {

    ob_start(); // Turn on output buffering
    ?>
    <section id="sectionlastworks" class="container">
    <h3 class="pre-title">Проекты</h3>
    <h2 class="title_horizontal">Последние <span>работы</span></h2>

        <div class="tab-last-works">
    <?php

    $categoryWorks = get_terms( 'sinworkcat' );

    if( $categoryWorks && ! is_wp_error($categoryWorks) ) {

        foreach( $categoryWorks as $cat ){
          //  var_dump($cat);
           if ($cat === $categoryWorks[0]) {
                echo "<button class=\"tablinks active\" onclick=\"openTab(event, 'lastwork".$cat->term_id."' , 'content-last-works', 'tab-last-works')\">".$cat->name."</button>";
               }
               else {
                   echo "<button class=\"tablinks\" onclick=\"openTab(event, 'lastwork".$cat->term_id."' , 'content-last-works', 'tab-last-works')\">".$cat->name."</button>";
               }
           }
           echo "</div>";

    }
    else {
        return '<h3>Работ пока нет</h3>';
    }



    $posts = get_posts( array(
        'numberposts' => 16,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'include'     => array(),
        'exclude'     => array(),
        'meta_key'    => '',
        'meta_value'  =>'',
        'post_type'   => 'sinwork',
        'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
    ) );

    foreach( $posts as $post ){
        //var_dump($post);
        $post->terms = wp_get_post_terms( $post->ID, 'sinworkcat', array('fields' => 'ids') );
        //var_dump($post);
    }


    foreach( $categoryWorks as $cat ){

        if ($cat === $categoryWorks[0]) {
            echo "<div id=\"lastwork".$cat->term_id."\" class=\"content-last-works\" style=\"display: block;\">
            <div class=\"last-works__cards-group\">";
        } else {
            echo "<div id=\"lastwork".$cat->term_id."\" class=\"content-last-works\">
            <div class=\"last-works__cards-group\">";
        }


        foreach( $posts as $post ){

            foreach( $post->terms as $term ){
                if ($term === $cat->term_id) {

                    echo "<div class=\"last-works__card\">

                    <img src=\"".get_the_post_thumbnail_url($post->ID, 'thumbnail')."\" alt=\"\">

                    <div class=\"last-works__details\">
                        <h4>".$post->post_title."</h4>
                        <p>".$post->post_excerpt."</p>
                        <a href=\"".get_post_permalink( $post->ID )."\">Подробнее</a>
                    </div>

                </div>";

                }

            }
        }


        echo "</div></div>";

    }



    /* BEGIN HTML OUTPUT */
    ?>


        <div class="btn-group__center">
            <a href="works" class="btn-secondary">Все работы</a>
        </div>

    </section>


    <?php
    /* END HTML OUTPUT */

    $output = ob_get_contents(); // collect output
    ob_end_clean(); // Turn off ouput buffer

    return $output; // Print output
}
