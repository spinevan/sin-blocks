/**
 * BLOCK: react-lifecycle-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

import { RichText } from '@wordpress/block-editor';
const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { Component } = wp.element; // Import the Component base class from the React.js abstraction

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'sin/facts', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'sin-blocks - Facts' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'sin-blocks — Facts' ),
		__( 'sin Facts' ),
		__( 'create-guten-block' ),
	],
	supports: {
		align: ['full']
	},
	attributes: {
		blockTitle: {
			type: 'string',
			default: 'Факты обо мне'
		},
		blockDetailText: {
			type: 'string',
			default: 'I’ve a dedicated team of Qualified Professionals with industrytand technical expertise.'
		},
		fact1: {
			type: 'object',
			default: { value: '90', text: 'Projects' }
		},
		fact2: {
			type: 'object',
			default: { value: '55', text: 'Happy Clients' }
		},
		fact3: {
			type: 'object',
			default: { value: '135', text: 'Working Hours' }
		},
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: class extends Component {

		//standard constructor for a component
		constructor() {
			super( ...arguments );

			// example how to bind `this` to the current component for our callbacks
			this.onTitleChange = this.onTitleChange.bind( this );
			this.onDetailTextChange = this.onDetailTextChange.bind( this );
			this.onChangeValueFact1 = this.onChangeValueFact1.bind( this );
			this.onChangeTextFact1 = this.onChangeTextFact1.bind( this );
			this.onChangeValueFact2 = this.onChangeValueFact2.bind( this );
			this.onChangeTextFact2 = this.onChangeTextFact2.bind( this );
			this.onChangeValueFact3 = this.onChangeValueFact3.bind( this );
			this.onChangeTextFact3 = this.onChangeTextFact3.bind( this );

			// some place for your state
			this.state = {};

		}

		onTitleChange( newContent ) {
			this.props.setAttributes( { blockTitle : newContent } );
		}

		onDetailTextChange( newContent ) {
			this.props.setAttributes( { blockDetailText : newContent } );
		}

		onChangeValueFact1 ( el) {
			this.props.setAttributes( { fact1 : { ...this.props.attributes.fact1, ...{ value: el } } } );
		}

		onChangeTextFact1 ( el) {
			this.props.setAttributes( { fact1 : { ...this.props.attributes.fact1, ...{ text: el } } } );
		}

		onChangeValueFact2 ( el) {
			this.props.setAttributes( { fact2 : { ...this.props.attributes.fact2, ...{ value: el } } } );
		}

		onChangeTextFact2 ( el) {
			this.props.setAttributes( { fact2 : { ...this.props.attributes.fact2, ...{ text: el } } } );
		}

		onChangeValueFact3 ( el) {
			this.props.setAttributes( { fact3 : { ...this.props.attributes.fact3, ...{ value: el } } } );
		}

		onChangeTextFact3 ( el) {
			this.props.setAttributes( { fact3 : { ...this.props.attributes.fact3, ...{ text: el } } } );
		}

		// edit: function (props) {
		// Creates a <p class='wp-block-cgb-block-react-lifecycle-block'></p>.
		render() {
			return (
				<section className="facts">
					<div className="container facts__tab">

						<div className="facts__left">
							<RichText tagName="h2" className="title_horizontal" onChange={ this.onTitleChange } value={ this.props.attributes.blockTitle } />
							<RichText tagName="h4" className="detail-text" onChange={ this.onDetailTextChange } value={ this.props.attributes.blockDetailText } />
						</div>

						<div className="facts__right">
							<div className="facts__right-item">
								<RichText
									tagName="h4"
									value={ this.props.attributes.fact1.value }
									onChange={ this.onChangeValueFact1 }
								/>
								<RichText
									tagName="p"
									className="facts__right-item__desc"
									value={ this.props.attributes.fact1.text }
									onChange={ this.onChangeTextFact1 }
								/>
							</div>
							<div className="facts__right-item">
								<RichText
									tagName="h4"
									value={ this.props.attributes.fact2.value }
									onChange={ this.onChangeValueFact2 }
								/>
								<RichText
									tagName="p"
									className="facts__right-item__desc"
									value={ this.props.attributes.fact2.text }
									onChange={ this.onChangeTextFact2 }
								/>
							</div>
							<div className="facts__right-item">
								<RichText
									tagName="h4"
									value={ this.props.attributes.fact3.value }
									onChange={ this.onChangeValueFact3 }
								/>
								<RichText
									tagName="p"
									className="facts__right-item__desc"
									value={ this.props.attributes.fact3.text }
									onChange={ this.onChangeTextFact3 }
								/>
							</div>

						</div>

					</div>
				</section>
			);
		}
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 * @return {string}  The block,
	 * @param {object} settings Block settings
	 */
	save: ( props ) => {
		return (
			<section className="facts">
				<div className="container facts__tab">

					<div className="facts__left">
						<RichText.Content tagName="h2" className="title_horizontal" value={ props.attributes.blockTitle } />
						<RichText.Content tagName="h4" className="detail-text" value={ props.attributes.blockDetailText } />
					</div>

					<div className="facts__right">
						<div className="facts__right-item">
							<h4><RichText.Content value={ props.attributes.fact1.value } /><span>+</span></h4>
							<RichText.Content tagName="p" className="facts__right-item__desc" value={ props.attributes.fact1.text } />
						</div>
						<div className="facts__right-item">
							<h4><RichText.Content value={ props.attributes.fact2.value } /><span>+</span></h4>
							<RichText.Content tagName="p" className="facts__right-item__desc" value={ props.attributes.fact2.text } />
						</div>
						<div className="facts__right-item">
							<h4><RichText.Content value={ props.attributes.fact3.value } /><span>+</span></h4>
							<RichText.Content tagName="p" className="facts__right-item__desc" value={ props.attributes.fact3.text } />
						</div>

					</div>

				</div>


			</section>
		);
	},
} );
