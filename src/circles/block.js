/**
 * BLOCK: react-lifecycle-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { Component } = wp.element; // Import the Component base class from the React.js abstraction
import { RichText } from '@wordpress/block-editor';

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'sin/circles', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'sin-blocks - Circles' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'sin-blocks — circles' ),
		__( 'sin CGB Example' ),
		__( 'create-guten-block' ),
	],
	supports: {
		align: ['full']
	},
	attributes: {
		blockTitle: {
			type: 'string',
			default: 'You Need To Be Hired a master of Photoshop, Illustrator & XD'
		},
		bottomText: {
			type: 'string',
			default: 'Текст под заголовка блока'
		},
		card1: {
			type: 'object',
			default: { value: '85', text: 'текст' }
		},
		card2: {
			type: 'object',
			default: { value: '75', text: 'текст' }
		},
		card3: {
			type: 'object',
			default: { value: '65', text: 'текст' }
		},
		card4: {
			type: 'object',
			default: { value: '55', text: 'текст' }
		},
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: class extends Component {

		//standard constructor for a component
		constructor() {
			super( ...arguments );

			// example how to bind `this` to the current component for our callbacks
			this.onTitleChange = this.onTitleChange.bind( this );
			this.onBottomTextChange = this.onBottomTextChange.bind( this );

			this.onCard1ValueChange = this.onCard1ValueChange.bind( this );
			this.onCard1TextChange = this.onCard1TextChange.bind( this );

			this.onCard2ValueChange = this.onCard2ValueChange.bind( this );
			this.onCard2TextChange = this.onCard2TextChange.bind( this );

			this.onCard3ValueChange = this.onCard3ValueChange.bind( this );
			this.onCard3TextChange = this.onCard3TextChange.bind( this );

			this.onCard4ValueChange = this.onCard4ValueChange.bind( this );
			this.onCard4TextChange = this.onCard4TextChange.bind( this );

			// some place for your state
			this.state = {};

		}

		onTitleChange( newContent ) {
			this.props.setAttributes( { blockTitle : newContent } );
		}

		onBottomTextChange( newContent ) {
			this.props.setAttributes( { bottomText : newContent } );
		}

		onCard1ValueChange( newContent ) {
			this.props.setAttributes( { card1 : { ...this.props.attributes.card1, value: newContent } } );
		}
		onCard1TextChange( newContent ) {
			this.props.setAttributes( { card1 : { ...this.props.attributes.card1, text: newContent } } );
		}

		onCard2ValueChange( newContent ) {
			this.props.setAttributes( { card2 : { ...this.props.attributes.card2, value: newContent } } );
		}
		onCard2TextChange( newContent ) {
			this.props.setAttributes( { card2 : { ...this.props.attributes.card2, text: newContent } } );
		}

		onCard3ValueChange( newContent ) {
			this.props.setAttributes( { card3 : { ...this.props.attributes.card3, value: newContent } } );
		}
		onCard3TextChange( newContent ) {
			this.props.setAttributes( { card3 : { ...this.props.attributes.card3, text: newContent } } );
		}

		onCard4ValueChange( newContent ) {
			this.props.setAttributes( { card4 : { ...this.props.attributes.card4, value: newContent } } );
		}
		onCard4TextChange( newContent ) {
			this.props.setAttributes( { card4 : { ...this.props.attributes.card4, text: newContent } } );
		}

		render() {
			return (
				<section className="circle-progress">
					<div className="container">
						<div className="circle-progress__top">
							<RichText tagName="h2" className="circle-progress__title" onChange={ this.onTitleChange } value={ this.props.attributes.blockTitle } />

							<div className="circle-progress__card-group">

								<div className="circle-progress__card">
									<div className="circle-progress__card__circle">
										<span><RichText onChange={ this.onCard1ValueChange } value={ this.props.attributes.card1.value } /></span>
										<svg
											className="circle-progress__progress-ring"
											width="140"
											height="140">
											<circle
												id="circleprogress1"
												className="progress-ring__circle"
												stroke={'#08B2DC'}
												strokeWidth={'4'}
												fill={'transparent'}
												r={'66'}
												cx={'70'}
												cy={'70'}
												val={'85'} />
										</svg>
									</div>
									<RichText tagName="div" className="circle-progress__text-detail" onChange={ this.onCard1TextChange } value={ this.props.attributes.card1.text } />
								</div>

								<div className="circle-progress__card">
									<div className="circle-progress__card__circle">
										<span><RichText onChange={ this.onCard2ValueChange } value={ this.props.attributes.card2.value } /></span>
										<svg
											className="circle-progress__progress-ring"
											width="140"
											height="140">
											<circle
												id="circleprogress1"
												className="progress-ring__circle"
												stroke={'#08B2DC'}
												strokeWidth={'4'}
												fill={'transparent'}
												r={'66'}
												cx={'70'}
												cy={'70'}
												val={'85'} />
										</svg>
									</div>
									<RichText tagName="div" className="circle-progress__text-detail" onChange={ this.onCard2TextChange } value={ this.props.attributes.card2.text } />
								</div>

								<div className="circle-progress__card">
									<div className="circle-progress__card__circle">
										<span><RichText onChange={ this.onCard3ValueChange } value={ this.props.attributes.card3.value } /></span>
										<svg
											className="circle-progress__progress-ring"
											width="140"
											height="140">
											<circle
												id="circleprogress1"
												className="progress-ring__circle"
												stroke={'#08B2DC'}
												strokeWidth={'4'}
												fill={'transparent'}
												r={'66'}
												cx={'70'}
												cy={'70'}
												val={'85'} />
										</svg>
									</div>
									<RichText tagName="div" className="circle-progress__text-detail" onChange={ this.onCard3TextChange } value={ this.props.attributes.card3.text } />
								</div>

								<div className="circle-progress__card">
									<div className="circle-progress__card__circle">
										<span><RichText onChange={ this.onCard4ValueChange } value={ this.props.attributes.card4.value } /></span>
										<svg
											className="circle-progress__progress-ring"
											width="140"
											height="140">
											<circle
												id="circleprogress1"
												className="progress-ring__circle"
												stroke={'#08B2DC'}
												strokeWidth={'4'}
												fill={'transparent'}
												r={'66'}
												cx={'70'}
												cy={'70'}
												val={'85'} />
										</svg>
									</div>
									<RichText tagName="div" className="circle-progress__text-detail" onChange={ this.onCard4TextChange } value={ this.props.attributes.card4.text } />
								</div>

							</div>

						</div>

						<div className="circle-progress__bottom">
							<RichText tagName="h3" className="circle-progress__bottom__title" onChange={ this.onBottomTextChange } value={ this.props.attributes.bottomText } />
						</div>

					</div>
				</section>
			);
		}
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 * @return {string}  The block,
	 * @param {object} settings Block settings
	 */
	save: ( props ) => {
		return (
			<section className="circle-progress">
				<div className="container">
					<div className="circle-progress__top">
						<RichText.Content tagName="h2" className="circle-progress__title" value={ props.attributes.blockTitle } />
						<div className="circle-progress__card-group">

							<div className="circle-progress__card">
								<div className="circle-progress__card__circle">
									<RichText.Content tagName="span" value={ props.attributes.card1.value } />
									<svg
										className="circle-progress__progress-ring"
										width="140"
										height="140">
										<circle
											id="circleprogress1"
											className="progress-ring__circle"
											stroke={'#08B2DC'}
											strokeWidth={'4'}
											fill={'transparent'}
											r={'66'}
											cx={'70'}
											cy={'70'}
											val={ String(props.attributes.card1.value).replace(/[^.\d]+/g,'').replace( /^([^\.]*\.)|\./g, '$1' ) } />
									</svg>
								</div>
								<RichText.Content tagName="div" className="circle-progress__text-detail" value={ props.attributes.card1.text } />
							</div>

							<div className="circle-progress__card">
								<div className="circle-progress__card__circle">
									<RichText.Content tagName="span" value={ props.attributes.card2.value } />
									<svg
										className="circle-progress__progress-ring"
										width="140"
										height="140">
										<circle
											id="circleprogress2"
											className="progress-ring__circle"
											stroke={'#08B2DC'}
											strokeWidth={'4'}
											fill={'transparent'}
											r={'66'}
											cx={'70'}
											cy={'70'}
											val={ String(props.attributes.card2.value).replace(/[^.\d]+/g,'').replace( /^([^\.]*\.)|\./g, '$1' ) } />
									</svg>
								</div>
								<RichText.Content tagName="div" className="circle-progress__text-detail" value={ props.attributes.card2.text } />
							</div>

							<div className="circle-progress__card">
								<div className="circle-progress__card__circle">
									<RichText.Content tagName="span" value={ props.attributes.card3.value } />
									<svg
										className="circle-progress__progress-ring"
										width="140"
										height="140">
										<circle
											id="circleprogress3"
											className="progress-ring__circle"
											stroke={'#08B2DC'}
											strokeWidth={'4'}
											fill={'transparent'}
											r={'66'}
											cx={'70'}
											cy={'70'}
											val={ String(props.attributes.card3.value).replace(/[^.\d]+/g,'').replace( /^([^\.]*\.)|\./g, '$1' ) } />
									</svg>
								</div>
								<RichText.Content tagName="div" className="circle-progress__text-detail"  value={ props.attributes.card3.text } />
							</div>

							<div className="circle-progress__card">
								<div className="circle-progress__card__circle">
									<RichText.Content tagName="span" value={ props.attributes.card4.value } />
									<svg
										className="circle-progress__progress-ring"
										width="140"
										height="140">
										<circle
											id="circleprogress4"
											className="progress-ring__circle"
											stroke={'#08B2DC'}
											strokeWidth={'4'}
											fill={'transparent'}
											r={'66'}
											cx={'70'}
											cy={'70'}
											val={ String(props.attributes.card4.value).replace(/[^.\d]+/g,'').replace( /^([^\.]*\.)|\./g, '$1' ) } />
									</svg>
								</div>
								<RichText.Content tagName="div" className="circle-progress__text-detail" value={ props.attributes.card4.text } />
							</div>

						</div>

					</div>

					<div className="circle-progress__bottom">
						<RichText.Content tagName="h3" className="circle-progress__bottom__title" value={ props.attributes.bottomText } />
					</div>

				</div>
			</section>
		);
	},
} );
