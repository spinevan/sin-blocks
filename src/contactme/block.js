/**
 * BLOCK: react-lifecycle-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';
import { TextControl } from '@wordpress/components';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { Component } = wp.element; // Import the Component base class from the React.js abstraction

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'sin/contactme', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Свяжитесь со мной' ), // Block title.
	icon: 'megaphone', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'widgets', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.

	// Enable or disable support for low-level features
	supports: {
		// Turn off ability to edit HTML of block content
		html: false,
		// Turn off reusable block feature
		reusable: false,
		// Add alignwide and alignfull options
		align: false
	},
	attributes: {
		formShortCode: {
			type: 'string',
		},
		APIKey: {
			type: 'string',
		},
		centerLat: {
			type: 'string',
		},
		centerLng: {
			type: 'string',
		},
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: class extends Component {

		//standard constructor for a component
		constructor() {
			super( ...arguments );
			// some place for your state
			this.onShortCodeChange = this.onShortCodeChange.bind( this );
			this.onAPIKeyChange = this.onAPIKeyChange.bind( this );
			this.oncenterLatChange = this.oncenterLatChange.bind( this );
			this.oncenterLngChange = this.oncenterLngChange.bind( this );
			this.state = {};

		}


		onShortCodeChange( el ) {
			this.props.setAttributes( { formShortCode : el } );
		}
		onAPIKeyChange( el ) {
			this.props.setAttributes( { APIKey : el } );
		}
		oncenterLatChange( el ) {
			this.props.setAttributes( { centerLat : el } );
		}
		oncenterLngChange( el ) {
			this.props.setAttributes( { centerLng : el } );
		}

		// edit: function (props) {
		// Creates a <p class='wp-block-cgb-block-react-lifecycle-block'></p>.
		render() {
			return (
				<div className={ this.props.className }>
					<p>— Свяжитесь со мной --</p>
					<p> Шорт код формы обратной связи
						<TextControl
							value={this.props.attributes.formShortCode}
							onChange={ this.onShortCodeChange }
						/>
					</p>
					<p> Google APIKEY
						<TextControl
							value={this.props.attributes.APIKey}
							onChange={ this.onAPIKeyChange }
						/>
					</p>
					<p> Центр карты Lat
						<TextControl
							value={this.props.attributes.centerLat}
							onChange={ this.oncenterLatChange }
						/>
					</p>
					<p> Центр карты Lng
						<TextControl
							value={this.props.attributes.centerLng}
							onChange={ this.oncenterLngChange }
						/>
					</p>
					<p>
						Форма обратной связи и карта
					</p>
				</div>
			);
		}
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 * @return {string}  The block,
	 * @param {object} settings Block settings
	 */
	save: ( props ) => {
		return null
	},
} );
