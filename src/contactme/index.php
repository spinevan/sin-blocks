<?php
/**
 * Created by PhpStorm.
 * User: sinicyn
 * Date: 30.01.2020
 * Time: 10:45
 */

namespace Sin\Blocks\ContactMe;

add_action('plugins_loaded', __NAMESPACE__ . '\register_dynamic_block');


function register_dynamic_block() {
    // Only load if Gutenberg is available.
    if (!function_exists('register_block_type')) {
        return;
    }

    // Hook server side rendering into render callback
    // Make sure name matches registerBlockType in ./index.js
    register_block_type('sin/contactme', array(
        'render_callback' => __NAMESPACE__ . '\render_dynamic_block'
    ));
}

function render_dynamic_block($attributes) {


    ob_start(); // Turn on output buffering
    ?>
    <section class="contact-me">

        <div class="container">

            <div class="contact-me__row">
                <div>
                    <h3 class="pre-title">Свяжитесь со мной</h3>
                    <h2 class="title_horizontal">Есть <span>проект?</span></h2>

    <?php
    echo $attributes['formShortCode'];
    /* BEGIN HTML OUTPUT */
    ?>


                </div>

                <div>
                    <div id="map" class="contact-me__map"></div>
                    <script>
						var map;
						function initMap() {
							map = new google.maps.Map(document.getElementById('map'), {
								center: {lat: <?php echo $attributes['centerLat']; ?>, lng: <?php echo $attributes['centerLng']; ?>},
								zoom: 10,
								disableDefaultUI: true
							});
						}
                    </script>
                    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $attributes['APIKey']; ?>&callback=initMap"
                            async defer></script>
                </div>


            </div>
        </div>
    </section>


    <?php
    /* END HTML OUTPUT */

    $output = ob_get_contents(); // collect output
    ob_end_clean(); // Turn off ouput buffer

    return $output; // Print output
}
