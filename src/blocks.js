/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

//import './block/block.js';
import './intro/block.js';
import './presentation/block.js';
import './facts/block.js';
import './workprocess/block.js';
import './circles/block.js';
import './lastworks/block.js';
import './contactme/block.js';
