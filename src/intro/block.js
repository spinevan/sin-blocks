/**
 * BLOCK: react-lifecycle-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';
import { RichText } from '@wordpress/block-editor';
import { LinkEditor } from '../Components/linkEditor';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { Component } = wp.element; // Import the Component base class from the React.js abstraction

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'sin/intro', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'sin-blocks - Intro' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'sin-blocks — Intro' ),
		__( 'sin Intro' ),
		__( 'Intro' ),
	],
	attributes: {
		blockTitle: {
			type: 'string',
			default: 'Creative Designer & Developer'
		},
		blockPreTitle: {
			type: 'string',
			default: 'Это пред заголовок'
		},
		blockDetail: {
			type: 'string',
			default: 'I’ve a dedicated team of Qualified Professionals with industry experience and technical expertise to manage a multitude of tasks.'
		},
		btnPrimary: {
			type: 'object',
			default: {
				title: 'Напишите ми',
				href: '#'
			}
		},
		btnSecondary: {
			type: 'object',
			default: {
				title: 'Скачать ЦВ',
				href: '#'
			}
		}
	},

	supports: {
		align: ['full']
	},


	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: class extends Component {

		//standard constructor for a component
		constructor() {
			super( ...arguments );
			//console.log( this.props.name, ': constructor()' );

			// example how to bind `this` to the current component for our callbacks
			this.onTitleChange = this.onTitleChange.bind( this );
			this.onPreTitleChange = this.onPreTitleChange.bind( this );
			this.onDetailChange = this.onDetailChange.bind( this );
			this.onBtnPrimaryTitleChange = this.onBtnPrimaryTitleChange.bind( this );
			this.onBtnPrimaryHrefChange = this.onBtnPrimaryHrefChange.bind( this );
			this.onBtnSecondaryTitleChange = this.onBtnSecondaryTitleChange.bind( this );
			this.onBtnSecondaryHrefChange = this.onBtnSecondaryHrefChange.bind( this );
			// some place for your state
			this.state = {};

		}

		onTitleChange( newContent ) {
			this.props.setAttributes( { blockTitle : newContent } );
		}

		onPreTitleChange( newContent ) {
			this.props.setAttributes( { blockPreTitle : newContent } );
		}

		onDetailChange( newContent ) {
			this.props.setAttributes( { blockDetail : newContent } );
		}

		onBtnPrimaryTitleChange( newValue ) {

			this.props.setAttributes( {
				btnPrimary: { title: newValue,
					href: this.props.attributes.btnPrimary.href } }
			);
		}

		onBtnPrimaryHrefChange( newValue ) {

			this.props.setAttributes( {
				btnPrimary: { href: newValue,
					title: this.props.attributes.btnPrimary.title } }
			);
		}

		onBtnSecondaryTitleChange( newValue ) {

			this.props.setAttributes( {
				btnSecondary: { title: newValue,
					href: this.props.attributes.btnSecondary.href } }
			);
		}

		onBtnSecondaryHrefChange( newValue ) {

			this.props.setAttributes( {
				btnSecondary: { href: newValue,
					title: this.props.attributes.btnSecondary.title } }
			);
		}

		// edit: function (props) {
		// Creates a <p class='wp-block-cgb-block-react-lifecycle-block'></p>.
		render() {
			const btnPrimaryFields = [ { label: 'Текст кнопки', func: this.onBtnPrimaryTitleChange, value: this.props.attributes.btnPrimary.title },
				{ label: 'Ссылка', func: this.onBtnPrimaryHrefChange, value: this.props.attributes.btnPrimary.href }];
			const btnSecondaryFields = [ { label: 'Текст кнопки', func: this.onBtnSecondaryTitleChange, value: this.props.attributes.btnSecondary.title },
				{ label: 'Ссылка', func: this.onBtnSecondaryHrefChange, value: this.props.attributes.btnSecondary.href }];
			return (
				<section className="intro">

					<div className="container">

						<div className="intro__title">
							<div>
								<RichText tagName="h2" onChange={ this.onPreTitleChange } value={ this.props.attributes.blockPreTitle } />
								<RichText tagName="h1" onChange={ this.onTitleChange } value={ this.props.attributes.blockTitle } />
								<RichText tagName="h3" onChange={ this.onDetailChange } value={ this.props.attributes.blockDetail } />
							</div>

							<div className="btn-group">
								<a href="#0" className="btn-primary">
									<div className="sin-with-editor">
										<div>{ this.props.attributes.btnPrimary.title }</div>
										<LinkEditor className="sin-link-editor" fields={btnPrimaryFields} />
									</div>
								</a>
								<a href="#0" className="btn-secondary"><div className="sin-with-editor">
									<div>{ this.props.attributes.btnSecondary.title }</div>
									<LinkEditor className="sin-link-editor" fields={btnSecondaryFields} />
								</div></a>
							</div>
						</div>

					</div>

					<div className="polygon">

					</div>

				</section>
			);
		}
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 * @return {string}  The block,
	 * @param {object} settings Block settings
	 */
	save: ( props ) => {
		return (
			<section className="intro">

				<div className="container">

					<div className="intro__title">
						<div>

							<RichText.Content tagName="h2" value={ props.attributes.blockPreTitle } />
							<RichText.Content tagName="h1" value={ props.attributes.blockTitle } />
							<RichText.Content tagName="h3" value={ props.attributes.blockDetail } />

						</div>

						<div className="btn-group">
							<a href={props.attributes.btnPrimary.href} className="btn-primary">{props.attributes.btnPrimary.title}</a>
							<a href={props.attributes.btnSecondary.href} className="btn-secondary">{props.attributes.btnSecondary.title}</a>
						</div>
					</div>

				</div>

				<div className="polygon">

				</div>

			</section>
		);
	},
} );
