/**
 * BLOCK: react-lifecycle-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { Component } = wp.element; // Import the Component base class from the React.js abstraction

import { RichText, MediaPlaceholder } from '@wordpress/block-editor';
import { TextControl, IconButton } from '@wordpress/components';

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'sin/workprocess', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'sin-blocks - work process' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'sin-blocks — workprocess' ),
		__( 'work process' ),
		__( 'create-guten-block' ),
	],
	supports: {
		align: ['full']
	},
	attributes: {
		blockTitle: {
			type: 'string',
			default: 'Мой рабочий процесс'
		},
		blockPreTitle: {
			type: 'string',
			default: 'Как я работаю?'
		},
		tabs: {
			type: 'array',
			default: [
				{ title: 'Tab 1', textDetail: 'It is a long established fact that', imgsrc: 'images/media-button.png', link: '#', label: 'Дизайн1' },
				{ title: 'Tab 2', textDetail: 'It is a long established fact that', imgsrc: 'images/media-button.png', link: '#', label: 'Разработка2' },
				{ title: 'Tab 3', textDetail: 'It is a long established fact that', imgsrc: 'images/media-button.png', link: '#', label: 'Резил3' }
			]
		},
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: class extends Component {

		//standard constructor for a component
		constructor() {
			super( ...arguments );
			//console.log( this.props.name, ': constructor()' );

			// example how to bind `this` to the current component for our callbacks
			this.onTitleChange = this.onTitleChange.bind( this );
			this.onPreTitleChange = this.onPreTitleChange.bind( this );
			this.onClickTab = this.onClickTab.bind( this );

			this.onTabTitleChange = this.onTabTitleChange.bind( this );
			this.onTabTextDetailChange = this.onTabTextDetailChange.bind( this );
			this.onTabLinkChange = this.onTabLinkChange.bind( this );
			this.onTabLabelChange = this.onTabLabelChange.bind( this );
			this.handleSelectTabImage = this.handleSelectTabImage.bind( this );
			// some place for your state

			const openedImageDialogs = [ false, false, false ];
			this.state = { tabs : [...this.props.attributes.tabs], curTabElement: this.props.attributes.tabs[ 0 ],  openedImageDialogs: openedImageDialogs };

		}

		onTitleChange( newContent ) {
			//console.log(event.target.value);
			this.props.setAttributes( { blockTitle : newContent } );
		}

		onPreTitleChange( newContent ) {
			//console.log(event.target.value);
			this.props.setAttributes( { blockPreTitle : newContent } );
		}

		onClickTab( item ) {
			this.setState(
				{ curTabElement: item }
			);
		}

		onTabTitleChange( newContent ) {
			const curTabEl = this.state.curTabElement;
			curTabEl.title = newContent;
			const arrTabs = this.state.tabs;
			const index = arrTabs.indexOf(this.state.curTabElement);
			arrTabs[ index ] = curTabEl;
			this.setState(
				{
					curTabElement: curTabEl
				}
			);
			this.props.setAttributes( { tabs : [...arrTabs] } );
		}

		onTabTextDetailChange( newContent ) {
			const curTabEl = this.state.curTabElement;
			curTabEl.textDetail = newContent;
			const arrTabs = this.state.tabs;
			const index = arrTabs.indexOf(this.state.curTabElement);
			arrTabs[ index ] = curTabEl;
			this.setState(
				{
					curTabElement: curTabEl
				}
			);
			this.props.setAttributes( { tabs : [...arrTabs] } );
		}

		onTabLinkChange( el ) {
			const curTabEl = this.state.curTabElement;
			curTabEl.link = el;
			const arrTabs = this.state.tabs;
			const index = arrTabs.indexOf(this.state.curTabElement);
			arrTabs[ index ] = curTabEl;
			this.setState(
				{
					curTabElement: curTabEl
				}
			);
			this.props.setAttributes( { tabs : [...arrTabs] } );
		}

		onTabLabelChange( el ) {
			const curTabEl = this.state.curTabElement;
			curTabEl.label = el;
			const arrTabs = this.state.tabs;
			const index = arrTabs.indexOf(this.state.curTabElement);
			arrTabs[ index ] = curTabEl;
			this.setState(
				{
					curTabElement: curTabEl
				}
			);
			this.props.setAttributes( { tabs : [...arrTabs] } );
		}

		handleSelectTabImage( el ) {
			const curTabEl = this.state.curTabElement;
			curTabEl.imgsrc = el.url;
			const arrTabs = this.state.tabs;
			const index = arrTabs.indexOf(this.state.curTabElement);
			arrTabs[ index ] = curTabEl;
			this.setState(
				{
					curTabElement: curTabEl
				}
			);
			this.props.setAttributes( { tabs : [...arrTabs] } );
		}

		render() {

			const openModal = () => {
				const openedImageDialogs = this.state.openedImageDialogs;
				openedImageDialogs[ this.state.tabs.indexOf(this.state.curTabElement) ] = true;
				this.setState( { openedImageDialogs: [...openedImageDialogs] } )
			};
			const closeModal = () => {
				const openedImageDialogs = this.state.openedImageDialogs;
				openedImageDialogs[ this.state.tabs.indexOf(this.state.curTabElement) ] = false;
				this.setState( { openedImageDialogs: [...openedImageDialogs] } )
			};

			return (
				<section className="container">
					<RichText tagName="h3" className="pre-title" onChange={ this.onPreTitleChange } value={ this.props.attributes.blockPreTitle } />
					<RichText tagName="h2" className="title_horizontal" onChange={ this.onTitleChange } value={ this.props.attributes.blockTitle } />
					<div id="worktab1" className="tabcontent">
						<div className="row work-proc">
							<div className="col-50 work-proc__left-tab">
								<RichText tagName="h3" className="title_vertical" onChange={ this.onTabTitleChange } value={ this.state.curTabElement.title } />
								<div>
									<RichText tagName="p" onChange={ this.onTabTextDetailChange } value={ this.state.curTabElement.textDetail } />
								</div>
								<a href={this.state.curTabElement.link} className="btn-primary work-proc__btn">Подробнее</a>
								<p>URL страницы с деталями</p>
								<TextControl
									value={this.state.curTabElement.link}
									onChange={ this.onTabLinkChange }
								/>
								<p>Заголовок вкладки</p>
								<TextControl
									value={this.state.curTabElement.label}
									onChange={ this.onTabLabelChange }
								/>
							</div>
							<div className="col-50 work-proc__right-tab">
								<img className="work-proc__img" src={this.state.curTabElement.imgsrc} alt="image1" onClick={openModal} />
								{ this.state.openedImageDialogs[ this.state.tabs.indexOf(this.state.curTabElement) ] && (
									<MediaPlaceholder

										onSelect={ this.handleSelectTabImage }
										allowedTypes={ [ 'image' ] }
										multiple={ false }
										labels={ { title: 'Выбор картинки' } }
										addToGallery={ true }
									>
										<IconButton	icon="dismiss" label="Закрыть выбор картинки" onClick={closeModal} />
									</MediaPlaceholder>
								) }
							</div>
						</div>
					</div>
					<div className="tab">
						{this.state.tabs.map((item, index) => {
							return (
								<button key={index}
									className={'tablinks' + (item.label === this.state.curTabElement.label ? ' active' : '' )}
									onClick={() => this.onClickTab(item)}
								>
									{item.label}
								</button>
							);
						})}
					</div>

				</section>
			);
		}
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 * @return {string}  The block,
	 * @param {object} settings Block settings
	 */
	save: ( props ) => {
		return (
			<section className="container">
				<RichText.Content tagName="h3" className="pre-title"  value={ props.attributes.blockPreTitle } />
				<RichText.Content tagName="h2" className="title_horizontal"  value={ props.attributes.blockTitle } />

				{props.attributes.tabs.map((item, index) => {
					return (
						<div key={index} id={'worktab'+(index+1)} className="tabcontent" style={ (index === 0 ? { display: 'block' }: {}) }>
							<div className="row work-proc">
								<div className="col-50 work-proc__left-tab">
									<RichText.Content tagName="h3" className="title_vertical" value={ item.title } />
									<div>
										<RichText.Content tagName="p" onChange={ this.onTabTextDetailChange } value={ item.textDetail } />
									</div>
									<a href={item.link} className="btn-primary work-proc__btn">Подробнее</a>
								</div>
								<div className="col-50 work-proc__right-tab">
									<img className="work-proc__img" src={item.imgsrc} alt="image1" />
								</div>
							</div>
						</div>
					);
				})}

				<div className="tab">
					{props.attributes.tabs.map((item, index) => {
						return (
							<button key={index}
								className={'tablinks' + (index === 0 ? ' active' : '' )}
								id={'tablink'+(index+1)}
							>
								{item.label}
							</button>
						);
					})}
				</div>

			</section>
		);
	},
} );
